# XiVO-web-interface

This project is the core of the xivo web interface for administration.

# Development

Workflow:

1. Ensure you have all the requirements
1. Start development server
1. Start development build server

## Requirements

* A XiVO VM with latest version installed
* nodejs version 8.9.x
* npm version 5.5.x
* python
* xserver-xephyr for integration tests: `sudo apt install xserver-xephyr`
* openssh-server so you can access your files from your VM: `sudo apt install openssh-server`


## Docker

Dockerized web interface uses two containers, which must use docker volume to share directory 
/usr/share/xivo-web-interface. The volume can't be cleaned by removing containers. To clean it, run:

    xivo-dcomp stop nginx webi
    xivo-dcomp rm -f nginx webi
    docker volume rm xivo_webivol

## Developping on the web-interface

If your xivo is embedded in a virtual machine (VM), you may use one of the following methods to work on the web-interface
 from this project that you can clone on your computer. 

### With sshfs

Here's the process one can use to develop (you need to have sshfs installed on your computer and the xivo):
1. Clone projet
1. Boot xivo VM
1. On xivo VM, 'load' your own sources :
   ``` bash
   cd /usr/share
   # if there is a xivo-web-interface directory there, move it sideways while developping
   mv xivo-web-interface/ xivo-web-interface.ori
   # create the target directory
   mkdir xivo-web-interface
   # mount the sources
   sshfs -o allow_other <MY_USER>@<MY_COMPUTER_ID>:/path/to/project/xivo-web-interface/src xivo-web-interface
   ```
If this doesn't work, try using nfs instead. 
Else, go to the third part.

### With nfs

On computer:

1. Install nfs-kernel-server
1. Change owner of xivo-web-interface directory to nobody:nogroup
1. Add row to file /etc/exports

	/path/to/project/xivo-web-interface/src  XIVO_IP(rw,sync,no_subtree_check)

1. sudo systemctl restart nfs-kernel-server


On XiVO :
``` bash
    apt-get install nfs-common   
    mount -t nfs COMPUTER_IP:/path/to/project/xivo-web-interface/src /usr/share/xivo-web-interface
```
    
You will be able to unmount it later by using :
```bash
    umount -t nfs -a
```

Then go to the next part.

### With virtualbox shared folder

On host computer:

Configure a shared folder for your VM pointing to your xivo-web-interface src or dev environment:
* Path: /path/to/project/xivo-web-interface/src
* Name: xivo-webi

On XiVO VM:

1. Install VirtualBox Guest additions
2. Mount the xivo-webi folder: `mount -t vboxsf xivo-webi /usr/share/xivo-web-interface`

Then go to the next part.

### Docker  manipulations

Since the web-interface is embedded in docker, there are a few more steps to do before having your files displayed.
You need to tell the **nginx** and **webi** containers to work with the files you have mounted instead of the files they are currently using.

To do so : edit the compose file /etc/docker/xivo/docker-xivo.yml in your xivo VM
```yaml
volumes :
    - webivol:/usr/share/xivo-web-interface
# this is what you have actually displayed for the nginx and webi containers
```
```yaml
volumes : 
    - /usr/share/xivo-web-interface:/usr/share/xivo-web-interface
# this is what you should put instead 
```
Then update the containers to apply these modifications.
```bash
    xivo-dcomp restart webi nginx
    xivo-dcomp up -d
```
Now the containers will be looking for your files to mount instead of the ones in the volume. 

If you mounted whole src directory, you must run also the `npm i` and `npm run build-n-watch` commands on your PC (see below).

Note that ini files located in `webi_dockerized/docker` will not be mounted (they are not in src directory).

Don't forget : 
+ If you restart the VM, you need to mount your files once again in /usr/share/xivo-web-interface
+ When you have finished developping, put the compose file back in its previous state
+ The munin graphs are overriden by the dev mount, thus you won't see them. If you want to, you must recreate yourself the link the container tries to create itself inside. On your xivo VM, do :
```
mkdir -p /usr/share/xivo-web-interface/www/img/graphs/munin/localdomain/ && \
ln -s /var/www/munin/localdomain/localhost.localdomain/ /usr/share/xivo-web-interface/www/img/graphs/munin/localdomain/
```

### Starting development build server

Part of the project is using npm/webpack thus requiring you to start a build server to package dependencies, js & css files.

``` bash
npm i 
npm run build-n-watch (run from xivo-web-interface/src )
```

If you just want to check the build is ok before pushing, you can just run: `npm run build`

## Unit testing

### JS unit tests
``` bash
npm i
npm test
```

## Debugging after mounting your dev webi

See https://gitlab.com/avencall/randd-doc/-/wikis/coding/vscode

# Run Cypress Tests

you can run your tests from the xivo-cypress container 

```bash
 docker run -it  --network=host -v $PWD/cypress/e2e:/e2e/cypress/e2e -w /e2e xivoxc/xivo-cypress:VERSION
```

or you can run it locally by cloning the xivo.solutions/xivo-cypress repository