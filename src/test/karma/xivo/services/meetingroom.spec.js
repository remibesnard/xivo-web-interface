describe('meetingroom service', () => {
  var $httpBackend;
  var meetingroom;
  var $rootScope;

  var staticUrl = 'http://localhost:9876/configmgt/api/2.0/meetingrooms/static';
  var globalUrl = 'http://localhost:9876/configmgt/api/2.0/meetingrooms';

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject((_meetingRoom_, _$httpBackend_, _$rootScope_) => {
    $httpBackend = _$httpBackend_;
    meetingroom = _meetingRoom_;
    $rootScope = _$rootScope_;
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('checks if meetingroom name is not available', () => {
    let response = {"total":1,"list":[{"id":10,"name":"schup","displayName":"azerty","number":1213}]};
    $httpBackend.expectPOST(staticUrl + '/find').respond(response);
    let promise = meetingroom.displayNameIsAvailable('schup');
    $httpBackend.flush();
    promise.then((result) => {
      expect(result).toBe(false);
    });
  });

  it('checks if meetingroom name is available', () => {
    let response = {"total":0,"list":[]};
    $httpBackend.expectPOST(staticUrl + '/find').respond(response);
    let promise = meetingroom.displayNameIsAvailable('schub');
    $httpBackend.flush();
    promise.then((result) => {
      expect(result).toBe(true);
    });
  });

  it('checks if meetingroom number already exist', () => {
    let response = {"total":1,"list":[{"id":10,"name":"schup","displayName":"azerty","number":1213}]};
    $httpBackend.expectPOST(globalUrl + '/find').respond(response);
    let promise = meetingroom.numberIsAvailable(1213);
    $httpBackend.flush();
    promise.then((result) => {
      expect(result).toBe(false);
    });
  });

  it('checks if meetingroom number not already exist', () => {
    let response = {"total":0,"list":[]};
    $httpBackend.expectPOST(globalUrl + '/find').respond(response);
    let promise = meetingroom.numberIsAvailable(1214);
    $httpBackend.flush();
    promise.then((result) => {
      expect(result).toBe(true);
    });
  });

  it('doesnt count self as duplicate on number edit', () => {
    let response = {"total":1,"list":[{"id":10,"name":"schup","displayName":"azerty","number":1213}]};
    $httpBackend.expectPOST(globalUrl + '/find').respond(response);
    let promise = meetingroom.numberIsAvailable(1213, 10);
    $httpBackend.flush();
    promise.then((result) => {
      expect(result).toBe(true);
    });
  });

  it('doesnt count self as duplicate on name edit', () => {
    let response = {"total":1,"list":[{"id":10,"name":"schup","displayName":"azerty","number":1213}]};
    $httpBackend.expectPOST(staticUrl + '/find').respond(response);
    let promise = meetingroom.displayNameIsAvailable('schup', 10);
    $httpBackend.flush();
    promise.then((result) => {
      expect(result).toBe(true);
    });
  });
  
});