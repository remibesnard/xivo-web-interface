describe('dynamic-input directive', () => {
  var $compile;
  var $rootScope;
  var scope;
  var isolatedScope;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;

    let elem = angular.element('<dynamic-input></dynamic-input>');
    scope = $rootScope.$new();

    let elemCompiled = $compile(elem)(scope);
    $rootScope.$digest();

    isolatedScope = elemCompiled.isolateScope();
  }));


  it('adds item with max id of the current list when clicking on add button', () => {
    isolatedScope.inputs = [{id: 1}, {id: 3}, {id:6}];
    isolatedScope.columns = [{name:'a'}, {name:'b'}, {name:'c'}];

    isolatedScope.addNewInput();

    expect(isolatedScope.inputs).toEqual([{id: 1}, {id: 3}, {id:6}, {id:7, a: null, b:null, c:null}]);
  });


  it('removes the unwanted item when clicking on the remove button', () => {
    isolatedScope.inputs = [{id: 1}, {id: 3}, {id:6}];

    isolatedScope.removeInput({id: 3});

    expect(isolatedScope.inputs).toEqual([{id: 1}, {id:6}]);
  });

});
