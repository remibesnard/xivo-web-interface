describe('sortable-select directive', () => {
  var $compile;
  var $rootScope;
  var scope;
  var isolatedScope;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Xivo'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;

    let elem = angular.element('<sortable-select></sortable-select>');
    scope = $rootScope.$new();

    let elemCompiled = $compile(elem)(scope);
    $rootScope.$digest();

    isolatedScope = elemCompiled.isolateScope();
  }));

  it('adds item of the selected list when clicking on add button', () => {
    isolatedScope.list = [{id: 5}];
    isolatedScope.selected = [{id: 1}, {id: 3}, {id:6}];

    isolatedScope.add({id: 5});

    expect(isolatedScope.selected).toEqual([{id: 1}, {id: 3}, {id:6}, {id: 5}]);
    expect(isolatedScope.list).toEqual([]);
  });

  it('removes the unwanted selected item when clicking on the remove button', () => {
    isolatedScope.list = [];
    isolatedScope.selected = [{id: 1}, {id: 3}, {id:6}];

    isolatedScope.remove({id: 3});

    expect(isolatedScope.selected).toEqual([{id: 1}, {id:6}]);
    expect(isolatedScope.list).toEqual([{id: 3}]);
  });

  it('adds all items of the selected list when clicking on add all button', () => {
    isolatedScope.list = [{id: 1}, {id: 3}, {id:6}];
    isolatedScope.selected = [];

    isolatedScope.addAll();

    expect(isolatedScope.selected).toEqual([{id: 1}, {id: 3}, {id:6}]);
    expect(isolatedScope.list).toEqual([]);
  });

  it('removes the all unwanted selected items when clicking on the remove all button', () => {
    isolatedScope.selected = [{id: 1}, {id: 3}, {id:6}];
    isolatedScope.list = [];

    isolatedScope.removeAll();

    expect(isolatedScope.list).toEqual([{id: 1}, {id: 3}, {id:6}]);
    expect(isolatedScope.selected).toEqual([]);
  });

});
