describe('add mediaserver controller', () => {
  var $controller;
  var $rootScope;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.inject((_$controller_, _$rootScope_) => {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
  }));

  it('can be instantiated', () => {
    var $scope = $rootScope.$new();
    let ctrl = $controller('AddMediaServerController', { $scope: $scope });
    expect(ctrl).toBeDefined();
  });
});