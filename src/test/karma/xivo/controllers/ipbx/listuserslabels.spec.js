describe('listuserslabels controller', () => {
  var $controller;
  var $rootScope;
  var $q;

  var userLabels;
  var scope;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_$controller_,_$rootScope_, _$q_, _userLabels_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $q = _$q_;

    userLabels = _userLabels_;
    scope = $rootScope.$new();
  }));

  it('Checks that the controller exists', () => {
    var ctrl =$controller('ListUsersLabelsController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });

  it('transform label list to multiselect ngTable option list at init', () => {
    spyOn(userLabels, 'auth').and.callFake(() => {
      let defer = $q.defer();
      defer.resolve('token');
      return defer.promise;
    });

    spyOn(userLabels, 'listLabels').and.callFake(() => {
      let defer = $q.defer();
      defer.resolve({"data": {"items":
      [
          {"users_count": 2, "display_name": "blue", "id": 3, "description": null},
          {"users_count": 2, "display_name": "green", "id": 2, "description": null},
          {"users_count": 2, "display_name": "red", "id": 1, "description": null}
      ],
        "total": 3}});
      return defer.promise;
    });
    $controller('ListUsersLabelsController', {'$scope': scope});
    $rootScope.$digest();

    expect(scope.list).toEqual([{id:3, label:"blue"}, {id:2, label:"green"}, {id:1, label:"red"}]);
  });

});
