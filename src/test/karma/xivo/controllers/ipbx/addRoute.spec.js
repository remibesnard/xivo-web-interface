describe('add-route controller', () => {
  var $controller;
  var $rootScope;
  var $q;

  var scope;
  var ctrl;
  var route;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_$q_, _$controller_,_$rootScope_, _route_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $q = _$q_;

    scope = $rootScope.$new();
    route = _route_;
  }));

  it('Checks that the controller exists', () => {
    ctrl = $controller('IpbxAddRouteController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });

  it('gets a new template at instanciation', () => {
    spyOn(route, 'getTemplate').and.callFake(() => {
      return $q.defer().promise;
    });
    $rootScope.$digest();
    ctrl = $controller('IpbxAddRouteController', {'$scope': scope});

    expect(route.getTemplate).toHaveBeenCalled();
  });


});
