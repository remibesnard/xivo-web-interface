describe('listusers controller', () => {
  var $controller;
  var $rootScope;

  var scope;
  var ctrl;

  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function (_$controller_, _$rootScope_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;

    scope = $rootScope.$new();
  }));

  it('Checks that the controller exists', () => {
    ctrl = $controller('ListUsersController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });

  it('works correctly with the localstorage', () => {
    let localStorageMock = (function() {
      let store = {};
      return {
        getItem: function(key) {
          return store[key];
        },
        setItem: function(key, value) {
          store[key] = value.toString();
        },
        clear: function() {
          store = {};
        },
        removeItem: function(key) {
          delete store[key];
        }
      };
    })();
    Object.defineProperty(window, 'localStorage', { value: localStorageMock });
    ctrl = $controller('ListUsersController', {'$scope': scope});

    let result = ctrl.getResultCountPerPage();
    expect(result).toEqual('25');

    ctrl.setResultCountPerPage(10);
    result = ctrl.getResultCountPerPage();
    expect(result).toEqual('10');
  });

});
