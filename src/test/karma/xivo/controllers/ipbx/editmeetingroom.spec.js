describe('edit-meetingroom controller', () => {
  var $controller;
  var $rootScope;
  
  var scope;
  var ctrl;
  
  beforeEach(angular.mock.module('Xivo'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  
  beforeEach(angular.mock.inject(function(_$controller_,_$rootScope_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    
    scope = $rootScope.$new();
  }));
  
  it('Checks that the controller exists', () => {
    ctrl = $controller('EditMeetingroomController', {'$scope': scope});
    expect(ctrl).toBeDefined();
  });
  
});
