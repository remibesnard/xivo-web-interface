<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2018  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

dwho::load_class('dwho_http');
$http_response = dwho_http::factory('response');

$access_category = 'control_system';
$access_subcategory = 'authenticate';

if(xivo_user::chk_acl($access_category, $access_subcategory) === true) {
  $http_response->set_status_line(200);
  $http_response->send(true);
} else {
  $http_response->set_status_line(403);
  $http_response->send(true);
}

?>
