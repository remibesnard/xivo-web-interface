<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2019  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

dwho::load_class('dwho_prefs');
$prefs = new dwho_prefs('ivr');

$appincall = &$ipbx->get_application('ivr');
$appschedule = &$ipbx->get_application('schedule');
$apprightcall = &$ipbx->get_application('rightcall',null,false);

$_TPL->load_i18n_file('tpl/www/bloc/service/ipbx/asterisk/call_management/ivr.i18n', 'global');

$schedules = $appschedule->get_schedules_list();

$info = array();

$param = array();

$menu = &$_TPL->get_module('menu');
$menu->set_top('top/user/'.$_USR->get_info('meta'));
$menu->set_left('left/service/ipbx/'.$ipbx->get_name());
$menu->set_toolbar('toolbar/service/ipbx/'.$ipbx->get_name().'/call_management/ivr');

$_TPL->set_var('schedules',$schedules);

$_TPL->set_bloc('main','service/ipbx/'.$ipbx->get_name().'/call_management/ivr/main');
$_TPL->set_struct('service/ipbx/'.$ipbx->get_name());
$_TPL->display('index');

?>
