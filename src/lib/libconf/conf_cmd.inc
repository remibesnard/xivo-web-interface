<?php


$array = array();

# edit web services access
$array['accesswebservice'] = array();
$array['accesswebservice']['update_keys'] = array('reload');

# edit user or agent with or without line - using also confd if editing line
$array['userfeatures'] = array();
$array['userfeatures']['ctibus'] = array('xivo[user,%s,%s]');
$array['userfeatures']['ipbx'] = array('dialplan reload', 'module reload app_queue.so', 'sip reload');

# edit group
$array['groupfeatures'] = array();
$array['groupfeatures']['ctibus'] = array('xivo[group,%s,%s]');
$array['groupfeatures']['ipbx'] = array('dialplan reload', 'module reload app_queue.so', 'sip reload', 'module reload chan_sccp.so');

# not used - line is edited by confd
$array['linefeatures'] = array();
$array['linefeatures']['ctibus'] = array('xivo[phone,%s,%s]');
$array['linefeatures']['ipbx'] = array('module reload app_queue.so', 'dialplan reload', 'sip reload');

# edit conference room
$array['meetmefeatures'] = array();
$array['meetmefeatures']['ctibus'] = array('xivo[meetme,%s,%s]');
$array['meetmefeatures']['ipbx'] = array('dialplan reload', 'module reload app_queue.so');

# not used - voicemail is edited by confd
$array['voicemail'] = array();
$array['voicemail']['ctibus'] = array('xivo[voicemail,%s,%s]');
$array['voicemail']['ipbx'] = array('voicemail reload');

# edit agent - it also updates user
$array['agentfeatures'] = array();
$array['agentfeatures']['ctibus'] = array('xivo[agent,%s,%s]', 'xivo[queuemember,update]');
$array['agentfeatures']['agentbus'] = array('agent.%s.%s');
$array['agentfeatures']['ipbx'] = array('module reload app_queue.so');

# edit global advanced parameters (agentglobalparams, general, queues.conf, meetme.conf)
$array['agentglobalparams'] = array();
$array['agentglobalparams']['ctibus'] = array('xivo[cticonfig,update]');

# edit global advanced parameters (agentglobalparams, general, queues.conf, meetme.conf)
$array['general'] = array();
$array['general']['ipbx'] = array('sip reload');

# edit queue (queue, queuefeatures)
$array['queue'] = array();
$array['queue']['ipbx'] = array('dialplan reload', 'module reload app_queue.so', 'sip reload');

# edit queue (queue, queuefeatures)
$array['queuefeatures'] = array();
$array['queuefeatures']['ctibus'] = array('xivo[queue,%s,%s]', 'xivo[queuemember,update]');
$array['queuefeatures']['agentbus'] = array('queue.%s.%s');
$array['queuefeatures']['ipbx'] = array('module reload app_queue.so', 'dialplan reload', 'sip reload', 'module reload chan_sccp.so');

# edit skill
$array['queueskill'] = array();
$array['queueskill']['ipbx'] = array('module reload app_queue.so');

# edit skill rule
$array['queueskillrule'] = array();
$array['queueskillrule']['ipbx'] = array('module reload app_queue.so');

$array['cti*'] = array();
$array['cti*']['ctibus'] = array('xivo[cticonfig,update]');

# edit call filter (callfilter, callfiltermember)
$array['callfilter'] = array();
$array['callfilter']['ipbx'] = array('dialplan reload');

# edit call filter (callfilter, callfiltermember)
$array['callfiltermember'] = array();
$array['callfiltermember']['ipbx'] = array('dialplan reload');

# edit incoming call
$array['incall'] = array();
$array['incall']['ipbx'] = array('dialplan reload');

# not used
$array['outcall'] = array();
$array['outcall']['ipbx'] = array('dialplan reload');

# edit context
$array['context'] = array();
$array['context']['ipbx'] = array('dialplan reload');

# edit call pickup
$array['pickup'] = array();
$array['pickup']['ipbx'] = array('sip reload', 'module reload chan_sccp.so');

$array['staticsip'] = array();
$array['staticsip']['ipbx'] = array('sip reload');

# edit sip trunk
$array['trunksip'] = array();
$array['trunksip']['ipbx'] = array('dialplan reload', 'sip reload');

$array['extensions'] = array();
$array['extensions']['ipbx'] = array('dialplan reload', 'module reload features');

# not used
$array['musiconhold'] = array();
$array['musiconhold']['ipbx'] = array('moh reload');

$array['sccp*'] = array();
$array['sccp*']['ipbx'] = array('module reload chan_sccp.so');

# edit configuration files
$array['configfiles'] = array();
$array['configfiles']['ipbx'] = array('dialplan reload');

# REALSTATIC FILE

# edit global advanced parameters (agentglobalparams, general, queues.conf, meetme.conf)
$array['queues.conf'] = array();
$array['queues.conf']['ctibus'] = array('xivo[cticonfig,update]');
$array['queues.conf']['ipbx'] = array('module reload app_queue.so');

# edit global advanced parameters (agentglobalparams, general, queues.conf, meetme.conf)
$array['meetme.conf'] = array();
$array['meetme.conf']['ctibus'] = array('xivo[cticonfig,update]');
$array['meetme.conf']['ipbx'] = array('dialplan reload', 'module reload app_meetme.so');

# edit voicemails in general settings
$array['voicemail.conf'] = array();
$array['voicemail.conf']['ipbx'] = array('voicemail reload');

# edit sip protocol in general settings
$array['sip.conf'] = array();
$array['sip.conf']['ipbx'] = array('sip reload','module reload res_rtp_asterisk.so');

# edit extensions
$array['features.conf'] = array();
$array['features.conf']['ipbx'] = array('dialplan reload', 'module reload features', 'module reload res_parking.so');

?>
