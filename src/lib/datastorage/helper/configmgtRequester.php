<?php

class configmgtHelper {

    const CONFIGMGT_HOST = "http://config_mgt:9000/configmgt/";

    static function get_services($userId, $token) {
		return self::_request_services($userId, "GET", $token);
    }

    static function update_services($userId, $services, $token) {
		return self::_request_services($userId, "PUT", $token, $services);
    }

    static function get_lines($user_id, $token)
	{
		return self::_request_line($user_id, 'GET', $token);
    }
    
    static function create_line($user_id, $token, $lines) {
        return self::_request_line($user_id, 'POST', $token, $lines);
    }

    static function update_line($user_id, $token, $lines) {
        return self::_request_line($user_id, 'PUT', $token, $lines);
    }
    
    static function delete_line($user_id, $token) {
		return self::_request_line($user_id, 'DELETE', $token);
    }
    
    private static function _request_services($user_id, $method, $token, $services = null) {
        $url = self::CONFIGMGT_HOST . "api/2.0/users/$user_id/services";
        return self::_request_configmgt($url, $method, $token, $services);
    }

    private static function _request_line($user_id, $method, $token, $lines = null) {
        $url = self::CONFIGMGT_HOST . "api/2.0/users/$user_id/line";
        return self::_request_configmgt($url, $method, $token, $lines);
	}

    private static function _request_configmgt($url, $method, $token, $data)
	{
        $ch = curl_init($url);
        if (isset($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
		curl_setopt($ch, CURLOPT_COOKIE, DWHO_SESS_STR);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("content-type: application/json", "X-Auth-Token: $token"));
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}
}

?>
