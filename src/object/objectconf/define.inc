<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2016  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


$file_version = '/usr/share/xivo/XIVO-VERSION';
$file_version_lts = '/usr/share/xivo/XIVO-VERSION-LTS';
$xivo_version = 'Unknown';

if (file_exists($file_version))
	$xivo_version = trim(file_get_contents($file_version));

$xivo_version_num = $xivo_version;

if (file_exists($file_version_lts)) {
	$xivo_version_lts = trim(file_get_contents($file_version_lts));
	if (!empty($xivo_version_lts))
		$xivo_version = $xivo_version_lts." (".$xivo_version.")";
}

define('XIVO_PROVIDER_SIP_CONFIG_DIR','/etc/xivo/provider-configuration');
define('XIVO_PROVIDER_SIP_CONFIG_MAX_BYTES','200000');
define('DWHO_LABEL_CUSTOM','XIVO-WEBI');

define('XIVO_SOFT_NAME','xivo');
define('XIVO_SOFT_LABEL','XIVO');
define('XIVO_SOFT_VERSION',$xivo_version);
define('XIVO_SOFT_URL','www.xivo.solutions');
define('XIVO_DOC_URL','documentation.xivo.solutions');
define('XIVO_DOC_API_URL','documentation.xivo.solutions');
define('XIVO_BLOG_URL','xivo-solutions-blog.gitlab.io');
define('XIVO_CORP_LABEL','Wisper');
define('XIVO_PF_FR_CORP_LABEL','Wisper');
define('XIVO_CORP_URL','wisper.io');

define('XIVO_AV_FR_CORP_NAME'		,'XiVO');
define('XIVO_AV_FR_CORP_EMAIL'		,'contact@wisper.io');
define('XIVO_AV_FR_CORP_URL'		,'www.wisper.io');
define('XIVO_AV_FR_CORP_ADDRESS'	,'31 chemin des Peupliers');
define('XIVO_AV_FR_CORP_ZIPCODE'	,'69570');
define('XIVO_AV_FR_CORP_CITY'		,'Dardilly');
define('XIVO_AV_FR_CORP_COUNTRY'	,'France');
define('XIVO_AV_FR_CORP_PHONE'		,'+33 (0) 4 37 49 78 10');
define('XIVO_AV_FR_CORP_FAX'		,'+33 (0) 4 27 46 63 30');

define('XIVO_WS_HEADER_NAME_VERSION','X-XIVO-WS-VERSION');
define('XIVO_WS_VERSION','1.0');

define('PROTOCOL_CUSTOM', 'custom');
define('PROTOCOL_SCCP', 'sccp');
define('PROTOCOL_WEBRTC', 'webrtc');
define('PROTOCOL_PHONE', 'phone');
define('PROTOCOL_UNIQUE_ACCOUNT', 'ua');
define('PROTOCOL_SIP', 'sip');


?>
