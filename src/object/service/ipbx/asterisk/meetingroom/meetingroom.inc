<?php

#
# XiVO Web-Interface
# Copyright (C) 2021  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

xivo_service_asterisk::required(array('abstract','abstract.inc'),true);

class xivo_service_asterisk_meetingroom extends xivo_service_asterisk_abstract
{
  var $_dso	= null;
  var $_name	= 'meetingroom';
  var $_filter	= false;
  var $_origin	= false;
  var $_identity	= false;

  public function __construct(&$sre,&$dso)
  {
    if(is_object($sre) === false)
      trigger_error('Invalid service in '.__CLASS__,E_USER_ERROR);

    if(is_object($dso) === false)
      trigger_error('Invalid datastorage in '.__CLASS__,E_USER_ERROR);

    $this->_sre = &$sre;
    $this->_dso = &$dso;

    $this->_load_config();
    $this->_identity = &$this->_sre->get_identity($this->_name);
  }

  function _prepare($data)
  {
    $this->_identity->_prepare_identity($data);

    $data['id']            = (int) $data['id'];
    $data['display_name']  = (string) $data['display_name'];
    $data['number']        = (string) $data['number'];
    $data['user_pin']      = (string) $data['user_pin'];
    return($data);
  }

  function get_all_except($id=0,$number=false,$order=null,$limit=null)
  {
    $where = array();
    $where['room_type'] = 'static';

    if(isset($this->_origin_list) === true)
      $this->_origin_list = false;

    if(($list = $this->_dso->get_all_where($where)) === false
    || isset($list[0]) === false)
      return(false);
    else if(isset($this->_origin_list) === true)
      $this->_origin_list = $list;

    return($this->_mk_list_assoc_prepare($list,false,true));
  }
}

?>
