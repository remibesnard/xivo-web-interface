<?php

#
# XiVO Web-Interface
# Copyright (C) 2016  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$url = &$this->get_module('url');

$result = $this->get_var('result');
$dbeg = $this->get_var('dbeg');
$dend = $this->get_var('dend');

if($result === false
|| is_null($dbeg) === true
|| is_null($dend) === true)
{
	die();
}

header('Pragma: no-cache');
header('Cache-Control: private, must-revalidate');
header('Last-Modified: '. date('D, d M Y H:i:s',mktime()).' '. dwho_i18n::strftime_l('%Z',null));
header('Content-Disposition: attachment; filename=xivo-switchboard-stats-from-'.$dbeg.'-to-'.$dend.'.pdf');
header('Content-Type: application/pdf');

ob_start();

echo $result;

header('Content-Length: '.ob_get_length());
ob_end_flush();
die();

?>
