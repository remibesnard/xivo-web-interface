<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2014  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$url = &$this->get_module('url');
$form = &$this->get_module('form');
$dhtml = &$this->get_module('dhtml');

$pager = $this->get_var('pager');
$act = $this->get_var('act');
$sort = $this->get_var('sort');

$col_name = $this->bbf('col_name');
$col_confno = $this->bbf('col_confno');
$col_pin = $this->bbf('col_pin');

$page = $url->pager($pager['pages'],
		    $pager['page'],
		    $pager['prev'],
		    $pager['next'],
		    'service/ipbx/pbx_settings/meetingroom',
		    array('act' => $act));

$entity_name = '';
if(defined('ENTITY_FILTER_NAME')):
	$entity_name = ENTITY_FILTER_NAME;
endif;

?>

<form action="#" name="fm-users-list" method="post" accept-charset="utf-8">
<?=$form->hidden(array('name' => DWHO_SESS_NAME,'value' => DWHO_SESS_ID))?>
<?=$form->hidden(array('name' => 'act','value' => $act))?>
<?=$form->hidden(array('name' => 'reboot','value' => ''))?>

	<div class="b-list">
		<div ng-controller="ListMeetingRoomsController as ctrl" ng-init="entity='<?php>print($entity_name); ?>'">
        <supertoolbar plus-action="ctrl.gotoAddMeetingroomPage" dropdown-actions="ctrl.supertoolbarDropdownActions" toolbar-error="toolbarError"></supertoolbar>
				<div class="pagination-no-top">
					<div ng-table-pagination="meetingRoomsTable" template-url="'ng-table/pager.html'"></div>
				</div>
        <table ng-table="meetingRoomsTable" class="table table-condensed table-striped-reverse table-hover table-bordered" show-filter="true">
            <tr ng-repeat="row in $data">
			        <td class="td-left">
			            <div class="form-group form-group-sm form-inline">
			                <div class="col-sm-3">
			                	<input type="checkbox" ng-click="ctrl.toggleBox(row.id, ctrl.selectedIds)" name="meetingRoom[]" id="it-meetingroom-{{row.id}}" class="it-checkbox" value="{{row.id}}">
			                </div>
			            </div>
			        </td>
					<td data-title="'<?php echo $col_name; ?>'" filter="{displayName: 'text'}" sortable="'displayName'"><b>{{row.displayName || "-"}}</b></td>
					<td data-title="'<?php echo $col_confno; ?>'" filter="{number: 'number'}">{{(row.number | translate) || "-"}}</td>
					<td data-title="'<?php echo $col_pin; ?>'" filter="{userPin: 'number'}">{{row.userPin || "-"}}</td>
			        </td>
			        <td class="td-right" data-title="'Actions'">
			            <a href="/service/ipbx/index.php/pbx_settings/meetingroom/?act=edit&amp;id={{row.id}}" title="Edit">
			                <img src="/img/site/button/edit.png" border="0" alt="Edit">
						</a>
			            <a ng-click="ctrl.deleteIds([row.id])" title="Delete">
			                <img src="/img/site/button/delete.png" border="0" alt="Delete">
			            </a>
			        </td>
	    	</tr>
				<tr ng-show="$data.length == 0">
					<td ng-if="isLoading" colspan="9" class="text-center">
						<i class="fa fa-spinner fa-pulse fa-fw"></i>
					</td>
	        <td ng-if="!isLoading" colspan="9" class="empty">
	            <?= $this->bbf('no_meetingroom'); ?>
	        </td>
				</tr>
			</table>
		</div>
	</div>
</form>