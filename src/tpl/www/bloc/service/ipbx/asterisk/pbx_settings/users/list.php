<?php

#
# XiVO Web-Interface
# Copyright (C) 2006-2016  Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$url = &$this->get_module('url');
$form = &$this->get_module('form');
$dhtml = &$this->get_module('dhtml');

$act = $this->get_var('act');

$ng_table_fullName = $this->bbf('ng_table_fullName');
$ng_table_provisioning = $this->bbf('ng_table_provisioning');
$ng_table_lineType = $this->bbf('ng_table_lineType');
$ng_table_phoneNumber = $this->bbf('ng_table_phoneNumber');
$ng_table_entity = $this->bbf('ng_table_entity');
$ng_table_mdsDisplayName = $this->bbf('ng_table_mdsDisplayName');

$entity_name = '';
if(defined('ENTITY_FILTER_NAME')):
	$entity_name = ENTITY_FILTER_NAME;
endif;

?>

<script type="text/ng-template" id="ng-table/filters/select-labels.html">
	<div ng-controller="ListUsersLabelsController">
		<multiple-select-dropdown options="list" selected-model="params.filter()[labels]" extra-settings="extraSettings"></multiple-select-dropdown>
	</div>
</script>

<form action="#" name="fm-users-list" method="post" accept-charset="utf-8">
<?=$form->hidden(array('name' => DWHO_SESS_NAME,'value' => DWHO_SESS_ID))?>
<?=$form->hidden(array('name' => 'act','value' => $act))?>
<?=$form->hidden(array('name' => 'reboot','value' => ''))?>

	<div class="b-list">
		<div ng-controller="ListUsersController as ctrl" ng-init="entity='<?php>print($entity_name); ?>'">
        <supertoolbar toolbar-error="toolbarError"></supertoolbar>
				<div class="pagination-no-top">
					<div ng-table-pagination="usersTable" template-url="'ng-table/pager.html'"></div>
				</div>
        <table ng-table="usersTable" class="table table-condensed table-striped-reverse table-hover table-bordered" show-filter="true">
            <tr ng-repeat="row in $data" ng-class="{'error-sccp' : row.lineType === 'sccp'}">
                <td class="td-left">
                    <div class="form-group form-group-sm form-inline">
                        <div class="col-sm-3">
                            <input type="checkbox" name="users[]" id="it-users-{{row.id}}" class="it-checkbox"
                                   value="{{row.id}}">
                        </div>
                    </div>
                </td>
                <td data-title="'<?php echo $ng_table_fullName; ?>'" filter="{fullName: 'text'}" sortable="'fullName'">
                    <img ng-src="{{ctrl.getStatusImg(row.disabled)}}" class="icons-list" alt="">
                    <b>{{row.fullName}}</b>
                </td>
                <td data-title="'<?php echo $ng_table_provisioning; ?>'" filter="{provisioning: 'number'}"
                    sortable="'provisioning'">{{row.provisioning || "-"}}
                </td>
                <td data-title="'<?php echo $ng_table_lineType; ?>'" filter="{lineType: 'select'}"
                    filter-data="getLineTypes()">{{(row.lineType | translate) || "-"}}
                </td>
                <td data-title="'<?php echo $ng_table_phoneNumber; ?>'" filter="{phoneNumber: 'text'}"
                    sortable="'phoneNumber'">{{row.phoneNumber || "-" }}
                </td>
                <td data-title="'<?php echo $ng_table_entity; ?>'" filter="{entity: 'text'}" sortable="'entity'">
                    {{row.entity}}
                </td>
                <td data-title="'<?php echo $ng_table_mdsDisplayName; ?>'" filter="{mdsDisplayName: 'text'}">
                    {{row.mdsDisplayName || "-"}}
                </td>
                <td data-title="'Labels'" filter="{labels: 'select-labels'}" filter-data="labels">
			            <span ng-if="row.labels.length" ng-repeat="label in row.labels">
			                {{label}}<span ng-show="!$last">,</span>
			            </span>
                    <span ng-if="!row.labels.length">-</span>
                </td>
                <td class="td-right" data-title="'Actions'">
                    <a ng-if="row.fullName != 'xuc technical'"
                       href="/service/ipbx/index.php/pbx_settings/users/?act=edit&amp;id={{row.id}}" title="Edit">
                        <img src="/img/site/button/edit.png" border="0" alt="Edit">
                    </a>
                    <a ng-if="row.fullName != 'xuc technical'"
                       href="/service/ipbx/index.php/pbx_settings/users/?act=delete&amp;id={{row.id}}"
                       onclick="return(confirm('<?php echo $this->bbf('opt_delete_confirm'); ?>'));" title="Delete">
                        <img src="/img/site/button/delete.png" border="0" alt="Delete">
                    </a>
                </td>
            </tr>
            <tr ng-show="$data.length == 0">
                <td ng-if="isLoading" colspan="9" class="text-center">
                    <i class="fa fa-spinner fa-pulse fa-fw"></i>
                </td>
                <td ng-if="!isLoading" colspan="9" class="empty">
                    <?= $this->bbf('no_user'); ?>
                </td>
            </tr>
        </table>
        </div>
    </div>
</form>
