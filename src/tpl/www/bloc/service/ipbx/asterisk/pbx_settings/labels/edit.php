<div class="b-infos b-form container-fluid">
  <breadcrumb parent="{{'labels' | translate}}" page="{{'edit' | translate}}"></breadcrumb>
  <div ng-controller="EditLabelsController as ctrl">
    <supertoolbar toolbar-error="toolbarError"></supertoolbar>
    <form action="" class="form-horizontal" accept-charser="utf-8" name="editLabelForm">
      <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : editLabelForm.name.$invalid && !editLabelForm.name.$pristine }">
        <div ng-show="editLabelForm.name.$invalid && !editLabelForm.name.$pristine" class="fm-error-icon">{{'fm_field_required' | translate}}</div>
      </div>
              
        <div class="form-group form-group-sm form-inline" ng-class="{ 'has-error' : editLabelForm.display_name.$invalid && !editLabelForm.display_name.$pristine }">
          <div class="row"> 
              <div class="col-sm-1"></div>
              <label for="it-display-name" class="control-label col-sm-2" id="lb-display-name">{{'display_name' | translate}}</label>
              <div class="col-sm-3">
                  <input type="text" name="display_name" id="it-display-name" class="form-control" ng-pattern="ctrl.name_validation" size=30 ng-model="label.display_name" required>
              </div> 
              <div class="col-sm-4">
                <div ng-messages="editLabelForm.display_name.$error">
                  <div ng-message="pattern" class="fm-error-icon">{{'name_validation' | translate}}</div>
                  <div ng-show="!editLabelForm.display_name.$pristine" ng-message="required" class="fm-error-icon">{{'fm_field_required' | translate}}</div> 
                </div>   
              </div> 
              <div class="col-sm-2"></div>
          </div>
        </div>

      <div class="form-group form-group-sm form-inline labels-description" ng-class="{ 'has-error' : editLabelForm.description.$invalid && !editLabelForm.description.$pristine }">
        <div class="row"> 
          <div class="col-sm-1"></div>
          <label for="it-description" class="control-label col-sm-2" id="lb-description">{{'description' | translate}}</label>
          <div class="col-sm-9">
            <textarea name="description" id="it-description" placeholder="{{'description_labels' | translate}}" class="form-control labels-textarea-description" rows="4" ng-model="label.description"></textarea>
          </div>
        </div>  
      </div>

      <p class="fm-paragraph-submit">
        <input ng-click="ctrl.validate()" ng-disabled="!editLabelForm.$valid" type="button" name="submit" id="it-submit" class="btn btn-primary it-submit" value="{{'fm_bt-save' | translate}}">
      </p>
    </form>
  </div>
</div>
