export default function meetingRoom($http) {
  
  const _serverUrl = window.location.protocol + '//' + window.location.host;
  const _staticMeetingRoomApi = _serverUrl + '/configmgt/api/2.0/meetingrooms/static';
  const _globalMeetingRoomApi = _serverUrl + '/configmgt/api/2.0/meetingrooms';

  const path = '/xivo/configuration/index.php/pbx_settings/meetingroom/';
  
  
  const getAllMeetingRooms = (query) => {
    return $http({
      url: _staticMeetingRoomApi,
      method: 'GET',
      data: query
    });
  };
  
  const getMeetingRoom = (id) => {
    return $http({
      url: _staticMeetingRoomApi + '/' + id,
      method: 'GET'
    });
  };
  
  const deleteMeetingRoom = (id) => {
    return $http({
      url: _staticMeetingRoomApi + '/' + id,
      method: 'DELETE'
    });
  };
  
  const createMeetingRoom = (meetingRoom) => {
    return $http({
      url: _staticMeetingRoomApi,
      method: 'POST',
      data: meetingRoom
    });
  };
  
  const updateMeetingRoom = (meetingRoom) => {
    return $http({
      url: _staticMeetingRoomApi,
      method: 'PUT',
      data: meetingRoom
    });
  };

  const getBaseUrlByReqType = (reqType) => {
    switch(reqType) {
    case 'static':
      return _staticMeetingRoomApi;
    case 'global':
      return _globalMeetingRoomApi;
    }
  };

  const filterMeetingRoom = (query, reqType = 'static') => {
    return $http({
      url: getBaseUrlByReqType(reqType) + '/find',
      method: 'POST',
      data: query
    });
  };
  
  const displayNameIsAvailable = (name, id) => {
    return checkAvailability('display_name', name, id, 'static');
  };

  const numberIsAvailable = (number, id) => {
    return checkAvailability('number', number, id, 'global');
  };

  const checkAvailability = (name, value, id, reqType) => {
    return filterMeetingRoom({
      "filters": [
        {
          "field": name,
          "operator": "=",
          "value": value
        }
      ],
      "offset": 0,
      "limit": 1
    }, reqType).then((resp) => {
      if (id && resp.data.total > 0) {
        return resp.data.list[0].id == id;
      }
      return resp.data.total == 0;
    });
  };
  
  const MeetingRoom = class MeetingRoom {
    constructor(id, displayName, number, userPin, roomType = 'static') {
      this.displayName = displayName;
      this.number = number;
      if (id) this.id = parseInt(id);
      if (userPin) this.userPin = userPin;
      this.roomType = roomType;
    }
  };
  
  return {
    getAllMeetingRooms: getAllMeetingRooms,
    getMeetingRoom: getMeetingRoom,
    deleteMeetingRoom: deleteMeetingRoom,
    createMeetingRoom: createMeetingRoom,
    updateMeetingRoom: updateMeetingRoom,
    MeetingRoom: MeetingRoom,
    filterMeetingRoom: filterMeetingRoom,
    displayNameIsAvailable: displayNameIsAvailable,
    numberIsAvailable: numberIsAvailable,
    path: path
  };
}
