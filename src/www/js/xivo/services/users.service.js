export default function users($http) {

  const _serverUrl = window.location.protocol + '//' + window.location.host;
  const _usersApi = _serverUrl + '/configmgt/api/2.0/users/find';

  const path = '/xivo/configuration/index.php/pbx_settings/users/';


  const filterUsers = (query) => {
    return $http({
      url: _usersApi,
      method: 'POST',
      data: query
    });
  };


  return {
    filterUsers: filterUsers,
    path: path
  };
}
