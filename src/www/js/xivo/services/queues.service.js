export default function queues($http) {

  const xivoApiUri = '/configmgt/api/1.0/recording/config/';

  const _recConfig = (action, queueId, content) => {
    return $http({
      url: xivoApiUri+queueId,
      method: action,
      data: content
    });
  };

  const _getRecConfig = (queueId) => {
    return _recConfig('GET', queueId);
  };

  const _setRecConfig = (queueId, data) => {
    return _recConfig('POST', queueId, data);
  };

  return {
    getRecConfig: _getRecConfig,
    setRecConfig: _setRecConfig
  };
}
