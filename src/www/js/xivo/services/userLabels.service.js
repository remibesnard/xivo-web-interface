export default function userLabels($http, $log) {
  
  const _serverUrl = window.location.protocol + '//' + window.location.host;
  const _authUrl = _serverUrl + '/auth/0.1/token';
  const _labelsApi = _serverUrl + '/conf/1.1/labels';

  const path = '/xivo/configuration/index.php/manage/labels/';

  let _authRequest = {
    url: _authUrl,
    method: 'POST',
    data: {
      "backend": "xivo_session",
      "expiration": 20
    }
  };

  const auth = async () => {
    let resp = await $http(_authRequest).catch(err => $log.error(err));
    return resp ? resp.data.data.token : '';
  };


  const listLabels = (token) => {
    return $http({
      url: _labelsApi,
      method: 'GET',
      headers: {
        "X-Auth-Token": token
      }
    });
  };

  const getLabel = (token, id) => {
    return $http({
      url: _labelsApi + '/' + id,
      method: 'GET',
      headers: {
        "X-Auth-Token": token
      }
    });
  };
  
  const deleteLabel = (token, id) => {
    return $http({
      url: _labelsApi + '/' + id,
      method: 'DELETE',
      headers: {
        "X-Auth-Token": token
      }
    });
  };
  
  const createLabel = (token, label) => {
    return $http({
      url: _labelsApi,
      method: 'POST',
      data: label,
      headers: {
        "X-Auth-Token": token
      }
    });
  };
  
  const updateLabel = (token, label, id) => {
    return $http({
      url: _labelsApi + '/' + id,
      method: 'PUT',
      data: label,
      headers: {
        "X-Auth-Token": token
      }
    });
  };
  
  return {
    listLabels: listLabels,
    getLabel: getLabel,
    deleteLabel: deleteLabel,
    createLabel: createLabel,
    updateLabel: updateLabel,
    auth: auth,
    path: path
  };
}
