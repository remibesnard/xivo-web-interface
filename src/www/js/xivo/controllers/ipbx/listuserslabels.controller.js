export default class ListUsersLabelsController {
  constructor($scope, $log, userLabels) {
    this.$scope = $scope;
    this.$log = $log;
    this.userLabels = userLabels;

    this.$scope.extraSettings = {
      scrollableHeight: '400px',
      scrollable: true,
      enableSearch: true
    };
    this.$scope.list = [];

    this.listLabelsAsNgTableOptions();
  }

  listLabelsAsNgTableOptions () {
    this.userLabels.auth().then(token => {
      this.userLabels.listLabels(token).then(listRequest => {
        this.$scope.list = listRequest.data.items.map(l => l = {id: l.id, label: l.display_name});
      }, (error) => {
        this.onError(error, 'fm_confd_error');
      });
    },(error) => {
      this.onError(error, 'fm_confd_error');
    });
  }

  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }
}
