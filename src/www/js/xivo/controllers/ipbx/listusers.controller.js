import RequestQueryBuilder from '../../services/RequestQueryBuilder';
import _ from 'lodash';

export default class ListUsersController {
  constructor(users, userLabels, $scope, $filter, $log, $translate, NgTableParams) {
    this.NgTableParams = NgTableParams;
    this.$scope = $scope;
    this.$log = $log;
    this.$filter = $filter;
    this.users = users;
    this.userLabels = userLabels;

    this.users.filterUsers({filters: [{field: "lineType", operator: "ilike", value: "%sccp%"}], offset: 0, limit: 25}).then( res => {
      if (res.data.total > 0) {
        this.onError($translate.instant('sccp_error'), $translate.instant('sccp_error'));
      }
    });


    $scope.labels = 'labels';

    $scope.getLineTypes = () => {
      return $translate(['phone', 'webrtc', 'ua', 'sccp', 'custom']).then (tr =>  {
        return [
          {id: 'phone', title: tr.phone},
          {id: 'webrtc', title: tr.webrtc},
          {id: 'ua', title: tr.ua},
          {id: 'sccp', title: tr.sccp},
          {id: 'custom', title: tr.custom}
        ];
      });
    };

    $scope.usersTable = new NgTableParams(
      {
        page: 1,
        count: this.getResultCountPerPage(),
      },
      {
        defaultSort: 'asc',
        getData: (params) => {
          $scope.isLoading = true;
          const currentQuery = this.generateQueryFilters(params);
          if (!_.isEqual(this.previousQuery, currentQuery)) {
            
            return this.sendRequest(currentQuery)

            .then((r) => {
              params.total(r.data.total);
              $scope.isLoading = false;
              this.previousQuery = currentQuery;
              this.previousResult = r.data.list;
              return r.data.list;
            }, 

            (error) => {
              $scope.isLoading = false;
              this.onError(error, 'fm_generic_error');
            });

          } else return this.previousResult;
        }
      }
    );
  }

  async listLabels () {
    let token = await this.userLabels.auth();
    if (token) {
      return this.userLabels.listLabels(token)
      .then((listRequest) => {
        return listRequest ? _.flatMap(listRequest.data.items, l => l = {'id': l.display_name, 'title': l.display_name}) : [];
      },(error) => {
        this.onError(error, 'fm_confd_error');
      });
    } else {
      return [];
    }
  }

  generateQueryFilters (params) {
    let q = new RequestQueryBuilder();
    q.limit(this.setResultCountPerPage(params.count())).offset(params.count() * (params.page()-1));
    if (Object.keys(params.sorting()).length !== 0) q.sort(params.sorting());
    if (Object.keys(params.filter()).length !== 0) {
      let filters = params.filter();
      for (let property in filters) {
        if (filters[property] && filters[property].toString().length > 0) {
          if (property == 'labels') {
            q.filterContainsAll(property, _.flatMap(filters[property], p => p.label));
          } else {
            q.filterIlike(property, '%' + filters[property] + '%');
          }
        }
      }
    }
    if (this.$scope.entity !== '') {
      q.filterEq("entity.name", this.$scope.entity);
    }
    return q.build();
  }

  setResultCountPerPage (count) {
    localStorage.setItem('NG_TABLE_COUNT', count);
    return count;
  }

  getResultCountPerPage () {
    let resultCount = localStorage.getItem('NG_TABLE_COUNT');
    if (!resultCount) resultCount = '25';
    return resultCount;
  }

  getStatusImg (status) {
    return status ? "/img/site/flag/disable.png" : "/img/site/flag/enable.png";
  }

  sendRequest (query) {
    return this.users.filterUsers(query);
  }

  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

}
