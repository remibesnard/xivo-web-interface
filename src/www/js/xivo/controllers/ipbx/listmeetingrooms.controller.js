import RequestQueryBuilder from '../../services/RequestQueryBuilder';
import _ from 'lodash';

export default class ListMeetingRoomsController {
  constructor(meetingRoom, $scope, $filter, $log, $translate, NgTableParams, $window) {
    this.NgTableParams = NgTableParams;
    this.$translate = $translate;
    this.$scope = $scope;
    this.$log = $log;
    this.$filter = $filter;
    this.$window = $window;
    this.meetingRoom = meetingRoom;
    this.gotoAddMeetingroomPage = this.gotoAddMeetingroomPage.bind(this);
    this.selectedIds = [];
    this.supertoolbarDropdownActions = [
      {
        label: 'delete_selection',
        func: this.deleteIds.bind(this, this.selectedIds)
      }
    ];

    $scope.meetingRoomsTable = new NgTableParams(
      {
        page: 1,
        count: this.getResultCountPerPage(),
      },
      {
        defaultSort: 'asc',
        getData: (params) => {
          $scope.isLoading = true;
          const currentQuery = this.generateQueryFilters(params);
          if (!_.isEqual(this.previousQuery, currentQuery)) {
            
            return this.sendRequest(currentQuery)
            
            .then((r) => {
              params.total(r.data.total);
              $scope.isLoading = false;
              this.previousQuery = currentQuery;
              this.previousResult = r.data.list;
              return r.data.list;
            }, 
            
            (error) => {
              $scope.isLoading = false;
              this.onError(error, 'fm_generic_error');
            });
            
          } else return this.previousResult;
        }
      }
    );
  }

  toggleBox (id, ids) {
    ids.includes(id) ? ids.splice(ids.indexOf(id), 1) : ids.push(id);
  }

  generateQueryFilters (params) {
    let q = new RequestQueryBuilder();
    q.limit(this.setResultCountPerPage(params.count())).offset(params.count() * (params.page()-1));
    if (Object.keys(params.sorting()).length !== 0) q.sort(params.sorting());
    if (Object.keys(params.filter()).length !== 0) {
      let filters = params.filter();
      for (let property in filters) {
        if (filters[property] && filters[property].toString().length > 0) {
          q.filterIlike(property, '%' + filters[property] + '%');
        }
      }
    }
    return q.build();
  }
    
  setResultCountPerPage (count) {
    localStorage.setItem('NG_TABLE_COUNT', count);
    return count;
  }
    
  getResultCountPerPage () {
    let resultCount = localStorage.getItem('NG_TABLE_COUNT');
    if (!resultCount) resultCount = '25';
    return resultCount;
  }
  
  sendRequest (query) {
    return this.meetingRoom.filterMeetingRoom(query);
  }

  gotoAddMeetingroomPage () {
    this.$window.location.href = './?act=add';
  }

  async deleteIds (ids) {
    if (ids.length >= 1 && confirm(this.$translate.instant(ids.length == 1 ? 'confirm_delete_meetingroom' : 'confirm_delete_meetingrooms'))) {
      try {
        for (let id of ids) {
          await this.meetingRoom.deleteMeetingRoom(id);
        }
        this.$window.location.reload();
      } catch (err) {
        this.$window.location.assign(this.$window.location.protocol + "//" + this.$window.location.host +
         this.$window.location.pathname + '?act=list&deleteError');
      }
    }
  }
  
  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

}
