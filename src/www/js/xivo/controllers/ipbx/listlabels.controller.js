
export default class ListLabelsController {
  constructor($scope, userLabels, $log, $window, $translate, NgTableParams) {
    this.$scope = $scope;
    this.$log = $log;
    this.$window = $window;
    this.$translate = $translate;
    this.userLabels = userLabels;
    this.NgTableParams = NgTableParams;
    this.gotoAddLabelPage = this.gotoAddLabelPage.bind(this);

    this.$scope.list = [];

    this.$scope.isLoading = true;

    this.$scope.headers = [
      'display_name',
      'description',
      'users_count'
    ];

    this.$scope.filters = {
      display_name: {display_name: 'text'}
    };

    this.$scope.actions = [
      'edit',
      'delete'
    ];

    this.$scope.path = '/service/ipbx/index.php/pbx_settings/labels/';

    this.selectedIds = [];

    this.supertoolbarDropdownActions = [
      {
        label: 'delete_selection',
        func: this.deleteIds.bind(this, this.selectedIds)
      }
    ];
    this.init(NgTableParams);
  }

  getLabels() {
    return this.$scope.list ? this.$scope.list : [];
  }

  onError (err, cause) {
    this.$log.error(err);
    this.$scope.toolbarError = cause;
  }

  async listLabels () {
    let token = await this.userLabels.auth();
    if (token) {
      return this.userLabels.listLabels(token)
      .then((listRequest) => {
        return listRequest ? listRequest.data.items : [];
      },(error) => {
        this.onError(error, 'fm_confd_error');
      });
    } else {
      this.$scope.toolbarError = 'fm_confd_error';
      this.$scope.$digest();
      return [];
    }
  }

  toggleBox (id, ids) {
    ids.includes(id) ? ids.splice(ids.indexOf(id), 1) : ids.push(id);
  }

  gotoAddLabelPage () {
    this.$window.location.href = './?act=add';
  }

  async delete(id) {
    try {
      await this.userLabels.deleteLabel(await this.userLabels.auth(), id);
      this.$window.location.reload();
    }
    catch (err) {
      this.$window.location.assign(this.$window.location.protocol + "//" + this.$window.location.host +
       this.$window.location.pathname + '?act=list&deleteError');
    }
  }

  async deleteIds (ids) {
    if (ids.length >= 1 && confirm(this.$translate.instant(ids.length == 1 ? 'confirm_delete_label' : 'confirm_delete_labels'))) {
      try {
        for (let id of ids) {
          await this.userLabels.deleteLabel(await this.userLabels.auth(), id);
        }
        this.$window.location.reload();
      } catch (err) {
        this.$window.location.assign(this.$window.location.protocol + "//" + this.$window.location.host +
         this.$window.location.pathname + '?act=list&deleteError');
      }
    }
  }

  async init (NgTableParams) {
    this.$scope.list = await this.listLabels();
    this.$scope.isLoading = false;
    this.$scope.tableParams = new NgTableParams({
      page: 1, // show first page
    }, {
      defaultSort: 'asc',
      filterDelay: 0,
      dataset: this.getLabels()
    });
    let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('deleteError')) {
      this.$scope.toolbarError = 'fm_confd_delete_error';
      var newurl = this.$window.location.protocol + "//" + this.$window.location.host + this.$window.location.pathname + '?act=list';
      window.history.pushState({path:newurl},'',newurl);
    }
    this.$scope.$digest();

  }
}
