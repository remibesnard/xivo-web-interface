export default function ctiClientFields() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/ctiClientFields.html',
    scope: {
      prechecked: '=',
      password: '=',
      username: '=',
    },
    link(scope) {
      scope.checked = scope.prechecked ? true : false;

      scope.ctiLoginActivated = () => { return scope.checked; };
    }
  };
}
