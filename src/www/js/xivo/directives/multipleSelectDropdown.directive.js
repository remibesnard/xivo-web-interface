export default function multipleSelectDropdown() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/multiple-select-dropdown.html',
    scope: {
      selectedModel: "=",
      options: "=",
      extraSettings: "="
    },
    controller: function($scope, $translate){
      $translate(['ms_checkall', 'ms_uncheckall', 'ms_selectioncount', 'ms_enablesearch', 'ms_disablesearch', 'ms_selectionof', 'ms_searchplaceholder',
        'ms_buttondefaulttext', 'ms_dynamicbuttontextsuffix', 'ms_selectgroup']).then (tr => $scope.mstranslation = {
          checkAll : tr.ms_checkall,
          uncheckAll : tr.ms_uncheckall,
          selectionCount : tr.ms_selectioncount,
          enableSearch : tr.ms_enablesearch,
          disableSearch : tr.ms_disablesearch,
          selectionOf : tr.ms_selectionof,
          searchPlaceholder : tr.ms_searchplaceholder,
          buttonDefaultText : tr.ms_buttondefaulttext,
          dynamicButtonTextSuffix: tr.ms_dynamicbuttontextsuffix,
          selectGroup: tr.ms_selectgroup
        }
      );

      $scope.modelWrapper = [];
      $scope.$watchCollection('modelWrapper', (newValue) => {
        $scope.selectedModel = newValue;
      });
    }
  };
}
