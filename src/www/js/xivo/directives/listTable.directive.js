export default function listTable() {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/list-table.html',
    scope: {
      headers: '=',
      list: '=',
      actions: '=',
      path: '=',
      loading: '=',
      formName: '@',
      delete: '&',
      deleteMessage: '@',
      noContentMessage: '@',
      withId: '=',
      showCheckbox: '=',
      checkboxToggleAction: '&'
    },
    link: (scope) => {

      scope.isArray = (value) => {
        return Array.isArray(value);
      };

      scope.getColSpan = () => {
        return scope.showCheckbox ? scope.headers.length + 2 : scope.headers.length + 1;
      };

      scope.columns = [];
      angular.forEach(scope.headers, (header) => {
        if (angular.isDefined(header.title)) {
          scope.columns.push(header);
        }
        else {
          scope.columns.push({title: header, dataKey: header});
        }
      });

    }
  };
}
