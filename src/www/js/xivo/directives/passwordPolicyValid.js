export default function passwordPolicyValid($window) {
  return {
    restrict: 'E',
    templateUrl: '/js/xivo/directives/passwordPolicyValid.html',
    scope: {
      regex: '=',
      password: '=',
      name: '=',
      policyLevel: '=',
      passwordId: '='
    },
    link(scope) {
      scope.passwordIsVisible = false;

      scope.genPass = () => {
        scope.password = $window.crypto
          .getRandomValues(
            new BigUint64Array(Math.floor(Math.random() * (4 - 2) + 2))
          )
          .reduce(
            (prev, curr, index) =>
              (!index ? prev : prev.toString(36)) +
              (index % 2
                ? curr
                  .toString(36)
                  .toUpperCase()
                  .replace(new RegExp(/\d/, "g"), (key) => "!@#$&(){}[]-.+".charAt(key))
                : curr.toString(36))
          )
          .split("")
          .sort(() => 128 - $window.crypto.getRandomValues(new Uint8Array(1))[0])
          .join("");
        if(!scope.passwordIsVisible) scope.togglePasswordVisibility();
      };

      scope.togglePasswordVisibility = () => {
        scope.passwordIsVisible = !scope.passwordIsVisible;
        let password = $window.document.getElementById(`it-${scope.passwordId}`);
        password.getAttribute("type") == "text" ? password.setAttribute("type", "password") : password.setAttribute("type", "text");
      };

      scope.sanitize = (password) => {
        if (password && password.length > 0) scope.password = password.replace(/[\\>'<"]+/g, '');
      };

      scope.validPolicy = new RegExp(scope.regex);

      scope.passwordPolicyValid = () => {
        return scope.validPolicy.test(scope.password);
      };


    }
  };
}