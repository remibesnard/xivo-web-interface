import {Common} from '../common';

let common = new Common();


describe('password securisation process Test', () => {

    beforeEach(() => {
        cy.visit('https://xivo-daily.dev.avencall.com/');
        common.login('root', 'superpass', '#it-login', '#it-password', '#it-submit')
        cy.wait(3000);
        cy.url().should('include', '/xivo/index.php');
    });

    describe('webservice test', () => {
        const goToWebServicePage = () => { 
            cy.visit('https://xivo-daily.dev.avencall.com/xivo/configuration/index.php/manage/accesswebservice/?act=add');
            cy.wait(3000);
            cy.get('#it-passwd').should('exist');
        }
    
        it('Should generate a password', () => {
            goToWebServicePage();
    
            cy.get('.icon-reset').click({force: true});
            cy.get('#it-passwd')
                .invoke('val')
                .should('match', /^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])(?=.*[\W_])[A-Za-z0-9\W_][^\"\\\'\<\>]{14,}$/);
        });
    
        it('Should show/hide password', () => {
            goToWebServicePage();
            
            cy.get('#it-passwd')
                .invoke('attr', 'type')
                .should('equal', 'password');

            cy.get('.icon-hide')
                .should('exist')
                .click();
            
            cy.get('#it-passwd')
                .invoke('attr', 'type')
                .should('equal', 'text');

            cy.get('.icon-eye')
                .should('exist')
                .click();
        });
    });


    describe('cti test', () => {
        const goToCtiPage = () => { 
            cy.visit('https://xivo-daily.dev.avencall.com/service/ipbx/index.php/pbx_settings/users/?act=add');
            cy.wait(3000);
            cy.get('#it-userfeatures-enableclient')
                .should('exist')
                .click();
        }

        it('Should generate a password', () => {
            goToCtiPage();
    
            cy.get('.icon-reset').click({force: true});
            cy.get('#it-userfeatures-passwdclient')
                .invoke('val')
                .should('match', /^(?=.*[A-Z])(?=.*[a-z])(?=.*?[0-9])[A-Za-z0-9\W_][^\"\\\'\<\>]{8,}$/);
        });

        it('Should show/hide password', () => {
            goToCtiPage();
            
            cy.get('#it-userfeatures-passwdclient')
                .invoke('attr', 'type')
                .should('equal', 'password');

            cy.get('.icon-hide')
                .should('exist')
                .click();

            cy.get('.icon-eye')
                .should('exist')
                .click();
        });
    
    })
    
});