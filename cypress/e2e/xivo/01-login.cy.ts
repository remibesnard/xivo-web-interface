import {Common} from '../common';

let common = new Common();

describe('Login process Test', () => {

    beforeEach(() => {
        cy.visit('https://xivo-daily.dev.avencall.com/');
    });

    it('Should Login!', () => {
        common.login('root', 'superpass', '#it-login', '#it-password', '#it-submit')
        cy.url().should('include', '/xivo/index.php');
    });

    it('Should not Login! - wrong login', () => {
        common.login('root2', 'superpass', '#it-login', '#it-password', '#it-submit')
        cy.url().should('not.include', '/xivo/index.php');
    });

    it('Should not Login! - wrong password', () => {
        common.login('root', 'superpassZ', '#it-login', '#it-password', '#it-submit')
        cy.url().should('not.include', '/xivo/index.php');
    });
});