import {Common} from '../common';

let common = new Common();

describe('1141 import Webrtc Ua Sip users', () => {

    let moveToUsersPage = () => {
        cy.get('.collapse.navbar-collapse > .nav.navbar-nav > li:first').click();
        cy.get('.collapse.navbar-collapse > .nav.navbar-nav > li:first > ul > li:first').click();
        cy.url().should('include', '/service/ipbx/index.php');

        cy.get('#mn-pbx-settings--users > a').click();
        cy.url().should('include', '/pbx_settings/users');
    }

    let moveToLineConfig = () => {
        cy.get('.sb-smenu > ul').contains('a', 'Lines').click()

        cy.get('#linefeatures > tr:first > td > a ').first().click()

        cy.get('.sb-smenu > ul').contains('a', 'Advanced').click()
    }

    beforeEach(() => {
        cy.visit('https://xivo-daily.dev.avencall.com/');
        common.login('root', 'superpass', '#it-login', '#it-password', '#it-submit')
        cy.url().should('include', '/xivo/index.php');
    });

    it('Import users!', () => {

        moveToUsersPage();

        cy.get('#toolbar-bt-add').click({force: true});
        cy.get('a[href*="./?act=import"]').click();

        cy.get('#it-import').click();

        cy.get('input[type="file"]').attachFile('import_user.csv');
        cy.get('#it-submit').click();

        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('106').should('exist');
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('107').should('exist');
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('108').should('exist');
    });

    it('Import Check Webrtc user!', () => {
        moveToUsersPage();

        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('106')
            .siblings().within(() => {
            cy.get('a[title="Edit"]').click()
        })

        moveToLineConfig()

        cy.get('#sip-options > tr').last().find('input[value="webrtc"]').should('exist');
        cy.get('#sip-options > tr').last().find('input[value="yes"]').should('exist');

    });

    it('Import Check SIP user!', () => {
        moveToUsersPage();

        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('107')
            .siblings().within(() => {
            cy.get('a[title="Edit"]').click()
        })

        moveToLineConfig()

        cy.get('#sip-options > tr').last().find('input[value="webrtc"]').should('not.exist');

    });


    it('Import Check UA user!', () => {
        moveToUsersPage();

        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('108')
            .siblings().within(() => {
            cy.get('a[title="Edit"]').click()
        })
        moveToLineConfig()

        cy.get('#sip-options > tr').last().find('input[value="webrtc"]').should('exist');
        cy.get('#sip-options > tr').last().find('input[value="ua"]').should('exist');
    });

    it('Delet users!', () => {
        moveToUsersPage();

        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('106')
            .siblings().within(() => {
            cy.get('a[title="Delete"]').click()
        })
        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('106').should('not.exist');
        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('107')
            .siblings().within(() => {
            cy.get('a[title="Delete"]').click()
        })
        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('107').should('not.exist');
        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('108')
            .siblings().within(() => {
            cy.get('a[title="Delete"]').click()
        })
        cy.wait(500);
        cy.get('table[ng-table="usersTable"]').find("tr").find("td").contains('108').should('not.exist');

    });

});
