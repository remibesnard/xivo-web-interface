export class Common {

    userToken: string = '';

    login(login: string, mdp: string, input1: string = '#input_0', input2: string = '#input_1', loginbutton: string = '#loginbutton') {
        cy.get(input1).type(login).should('have.value', login);
        cy.get(input2).type(mdp).should('have.value', mdp);
        cy.get(loginbutton).click();

    }

    initialSate() {
        cy.visit('http://localhost:8070/');
        cy.intercept('POST','/xuc/api/2.0/auth/login').as('postLogin') // and assign an alias

        this.login('ato', 'TEST');
        cy.wait('@postLogin')

        cy.url().should('include', '/ucassistant/favorites');
    }

    searchContact(contact: string) {
        cy.get('#search').type(contact).should('have.value', contact);
        cy.get('#xuc_search').click();
        cy.wait(4000);
    }


    expectPlayingAudio = (expectedValue: boolean) => {
        cy.get('audio,video').should((els:any)=>{
            let audible = false
            els.each((i:any, el:any)=>{
                console.log(el)
                console.log(el.duration, el.paused, el.muted)
                if (el.duration > 0 && !el.paused && !el.muted) {
                    audible = true
                }

                // expect(el.duration > 0 && !el.paused && !el.muted).to.eq(false)
            })
            expect(audible).to.eq(expectedValue)
        })
    }
}

