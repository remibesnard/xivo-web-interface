#!/usr/bin/env bash
set -e

if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

npm_build() {
    dir=$(pwd)
    cd ../src
    npm i
    npm run build
    rm -rf node_modules
    cd "$dir"
}

npm_build

docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/xivo-webi:$TARGET_VERSION -f docker/Dockerfile ..
